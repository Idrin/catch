﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchController : MonoBehaviour
{
    
    private PlayerMovementController PMC;
    private Material mat;

    public bool isHunter = false;
    

    void Start()
    {
        PMC = GetComponent<PlayerMovementController>();
        mat = GetComponent<Renderer>().material;
        if (isHunter)
        {
            mat.color = Color.red;
        }
        else
        {
            mat.color = Color.blue;
        }
    }
    
    public void catchPlayer()
    {
        isHunter = false;
        mat.color = Color.blue;
    }

    public void getCatched()
    {
        isHunter = true;
        mat.color = Color.red;
        StartCoroutine(catchFreeze());
    }

    private IEnumerator catchFreeze()
    {
        PMC.frozen = true;

        yield return new WaitForSeconds(2); //KillMePls this works

        PMC.frozen = false;
    }
}
