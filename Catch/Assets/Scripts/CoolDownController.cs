﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDownController : MonoBehaviour
{
    private bool isOnCooldown;
    private int coolDownTimer;

    // Start is called before the first frame update
    void Start()
    {
        isOnCooldown = false;
        coolDownTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (coolDownTimer > 0)
        {
            coolDownTimer--;
        }
    }

    public void triggerCooldown()
    {
        isOnCooldown = true;
        coolDownTimer = 100;
    }

    public bool getCooldownState()
    {
        return isOnCooldown;
    }
}
