﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public enum InputAxes
    {
        WASD = 0,
        ARROWS = 1
    }

    private CatchController _playerCatchControl;
    private CharacterController _charControl;
    private CoolDownController _coolDownController;
    private string playerHor = "";
    private string playerVer = "";
    private string playerTurbo = "";
    private bool isOnCooldown = false;

    public float boostDistance = 5f;
    public int coolDownTime = 1;
    public InputAxes playerInput = InputAxes.WASD;
    public float speed = 6f;
    public float gravity = -9.18f;
    public bool frozen = false;

    void Start()
    {
        //Components sammeln
        _playerCatchControl = GetComponent<CatchController>();
        _charControl = GetComponent<CharacterController>();
        //_coolDownController = GetComponent<CoolDownController>();
        Rigidbody body = GetComponent<Rigidbody>();

        //Freeze Body Rotation for Gravity
        if (body != null)
            body.freezeRotation = false;

        //Steuerung auswählen
        switch (playerInput)
        {
            case InputAxes.WASD:
                playerHor = "Horizontal";
                playerVer = "Vertical";
                playerTurbo = "space";
                break;
            case InputAxes.ARROWS:
                playerHor = "Horizontal2";
                playerVer = "Vertical2";
                playerTurbo = "[0]";
                break;
            default:
                Debug.Log("Something went wrong :(");
                break;
        }


    }

    void Update()
    {
        if (Input.anyKey && !frozen)
        {
            makeMove();
        }
    }

    void makeMove()
    {

        float deltaX = Input.GetAxis(playerHor) * speed;
        float deltaZ = Input.GetAxis(playerVer) * speed;
//        bool isOnCooldown = _coolDownController.getCooldownState();

        
        Vector3 move = new Vector3(deltaX/2+deltaZ/2, 0, deltaZ/2-deltaX/2);

        move = Vector3.ClampMagnitude(move, speed);     //Movement in diagonale gleich schnell
        
        move.y = gravity * Time.deltaTime;
        
        if (Input.GetKey(playerTurbo) && !isOnCooldown)
        {
            //move = sprint(move);      //Funktioniert theoretisch
            boost(move);            //Broken
        }

        move *= Time.deltaTime;                         //Movement unabhängig von FPS

        //move *= turbo;                            //TurboSpeed hinzufügen

        move = transform.TransformDirection(move);      //Local zu World Koordinaten
        
        _charControl.Move(move);

    }

    Vector3 sprint(Vector3 dir) //No Cooldown, but it works
    {
        Vector3 temp = dir;
        temp = Vector3.Scale(temp,new Vector3(2,0,2));
        return temp;
    }
    void boost(Vector3 dir)
    {
        Vector3 destination = Vector3.Scale(dir,new Vector3(boostDistance,0,boostDistance));
        destination = transform.TransformDirection(destination*Time.deltaTime);
        StartCoroutine(MoveToPosition(transform,destination,1.5f));
        StartCoroutine(coolDown());
        return;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        GameObject other = hit.transform.gameObject;
        CatchController otherCatchControl = other.GetComponent<CatchController>();
        if (otherCatchControl != null)
        {
            _playerCatchControl.catchPlayer();
            otherCatchControl.getCatched();
        }
    }

    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
   {
      var currentPos = transform.position;
      var t = 0f;
       while(t < 1)
       {
             t += Time.deltaTime / timeToMove;
             transform.position = Vector3.Lerp(currentPos, position, t);
             yield return null;
      }
    }
    private IEnumerator coolDown()
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(coolDownTime);
        isOnCooldown = false;
    }
}
